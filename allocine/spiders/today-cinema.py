import scrapy

from scrapy_rss import RssItem


class SomeSpider(scrapy.Spider):
    name = 'today-cinema'
    start_urls = ['http://www.allocine.fr/seance/salle_gen_csalle=B0065.html']

    def parse(self, response):
        films = response.xpath('//section[@class="section js-movie-list"]/div[@class="hred"]')
        for film in films:
            item = RssItem()

            name = film.xpath('.//a[@class="meta-title-link"]/text()')
            synopsis = film.xpath('.//div[@class="synopsis"]/text()')
            image_url = film.xpath('.//img/@data-src')
            url = film.xpath('.//a/@href')
            time = film.xpath('./div[@class="js-inline-showtime"]//text()')
                        
            item.title = name.extract_first()
            item.description = synopsis.extract_first() + '<br/>' + ''.join(time.extract())
            
            item.enclosure = {'url': image_url.extract_first(), 'type':'image/jpeg', 'length':0}
            item.link = 'http://www.allocine.fr'+url.extract_first()
            yield item
